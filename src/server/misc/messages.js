define([
	
], function(
	
) {
	return {
		login: {
			exists: 'username exists, please try another',
			allFields: 'please complete all fields',
			illegal: 'illegal characters in username/password',
			incorrect: 'invalid username and password',
			charExists: 'character name is taken'
		}	
	};
});